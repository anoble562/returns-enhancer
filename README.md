# Returns Enhancer #

Returns Enhancer is a suite of functionality built to extend Salesforce's native returns and depot management solutions. It leverages ShipEngine's logistics APIs to achieve the bulk of the tracking and label creation. More info here: https://www.shipengine.com/docs/getting-started/

See description below for a list of features.

### Version History ###

* 1.0 - Initial Build
* 2.0 - Added Depot Enhancer components

### Logistics ###

* Automated shipment tracking
* Automated shipping label creation
* Automated depot Work Order creation
* Dashboards and reports around common shipping metrics
* Filterable maps to track current shipment locations
* Shipment costing
* Automated B2C email communication (includes shipping labels as attachment)
* Print labels directly from Salesforce
* Supports split shipments
* Address validation and pre-fill
* Package templates for common shipment configurations

### Depot ###
* User-friendly UI (LWC) with seamless access to all related repair details
* Streamlined, customizable workflow
* Barcode scanning with automated Work Order lookup (tablet only)
* Speech to text dictation in app (tablet only)
* Capture images of devices and repair
* Templatized repair plans with AI-like recommendations based on keywords
* Repair wizards to guide repair technicians through complex workflows
* Includes reference images to aid repair technicians when words aren’t enough
* Automated Work Order completion
* Return label printing
* Suite of depot-focused metrics and KPIs


### Install ###

Package here: 