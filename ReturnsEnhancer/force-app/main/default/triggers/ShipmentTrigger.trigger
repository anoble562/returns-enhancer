trigger ShipmentTrigger on Shipment (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    ShipmentTriggerDispatcher.Run(new ShipmentHandler());
}