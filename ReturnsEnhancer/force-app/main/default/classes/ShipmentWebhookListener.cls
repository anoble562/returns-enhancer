/**
 * @File Name          : ShipmentWebhookListener.cls
 * @Description        : This class is used to process batch tracking updates to Shipments.
 * @Covered By         : TestClass.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

//https://alexlab-developer-edition.na174.force.com/shipmentwebhook/services/apexrest/shipmentwebhook/updateShipment

@RestResource(urlMapping='/shipments/*')
global class ShipmentWebhookListener {
	@HttpPost
	global static void handlePost() {

		RestResponse res = RestContext.response;
		
		try {
			String webhookBody = RestContext.request.requestBody.toString();
			

			if (webhookBody != null) {

				ShipmentWrapper.WebhookListenerWrap fullWrap = (ShipmentWrapper.WebhookListenerWrap)System.JSON.deserialize(webhookBody, ShipmentWrapper.WebhookListenerWrap.class);
				System.debug('Updates: '+fullWrap);

				ShipmentWrapper.TrackingRequest wrap = fullWrap.data;

				Shipment shipment = [SELECT Id FROM Shipment WHERE TrackingNumber = :wrap.tracking_number];

				if(wrap.actual_delivery_date != null){
					String actual_deliv = wrap.actual_delivery_date.formatGMT('yyyy-MM-dd HH:mm:ss');
					shipment.ActualDeliveryDate = Datetime.valueof(actual_deliv);
				}
				if(wrap.estimated_delivery_date != null){
					String est_deliv = wrap.estimated_delivery_date.formatGMT('yyyy-MM-dd HH:mm:ss');
					shipment.ExpectedDeliveryDate = Datetime.valueof(est_deliv);
				}
				
				shipment.Shipment_Details__c = wrap.carrier_status_description;
				System.debug('Events --- '+wrap.events);

				if(!wrap.events.isEmpty()){
					if(wrap.events[0].latitude != null && wrap.events[0].longitude != null){
					shipment.Last_Location__Latitude__s = Decimal.valueOf(wrap.events[0].latitude);
					shipment.Last_Location__Longitude__s = Decimal.valueOf(wrap.events[0].longitude);
					}
					shipment.Current_Postal_Code__c = wrap.events[0].postal_code;
					shipment.Current_City__c = wrap.events[0].city_locality;
					shipment.Current_State__c = wrap.events[0].state_province;
					shipment.Current_Country__c = wrap.events[0].country_code;
					shipment.Signor__c = wrap.events[0].signer;
					
				}else{
					shipment.Status = 'Unknown';
				}

				if(wrap.status_description.contains('Delivered')){
				shipment.Status = 'Delivered';
				}else if(wrap.status_description == 'In Transit' || wrap.status_description == 'Accepted'){
					shipment.Status = 'Shipped';
				}else{
					shipment.Status = 'Unknown';
				}

			shipment.Last_Integration_Update__c = Datetime.now();
			update shipment;

			res.statusCode = 200;
			res.responseBody = Blob.valueOf('Success! Updated '+shipment.Id);

			}

		} catch (Exception e) {
			System.debug('Error++ :'+e);
			res.statusCode = 500;
			res.responseBody = Blob.valueOf(e.getMessage());
		}
	}
}