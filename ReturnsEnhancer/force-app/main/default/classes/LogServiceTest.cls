@isTest
public class LogServiceTest {

    @isTest
    static void test() {

        Account account = ShipmentTestDataFactory.createAccount();

        Test.startTest();
        try{
            undelete account;
        }catch(Exception e){
        LogService.insertLog(account.Id, e);
        }
        Test.stopTest();

        Error_Logs__c log = [SELECT Id FROM Error_Logs__c WHERE Object_Id__c = :account.Id];

        System.assertNotEquals(null, log);

    }
}