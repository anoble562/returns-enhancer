/**
 * @File Name          : ShipmentServiceTest.cls
 * @Covers             : ShipmentService.cls, ShipmentServiceInvocable.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

@isTest
private class ShipmentServiceTest {

    @isTest
    static void testInTransit() {

        Shipment shipment = ShipmentTestDataFactory.createShipment();

        Test.setMock(HttpCalloutMock.class, new ShipmentTrackingHttpCalloutMock());
        Test.startTest();
        String shipmentId = (String) shipment.Id;
        ShipmentService.makeCallout(shipmentId,'fedex','123456');
        Test.stopTest();

        Shipment updatedShipment = [SELECT Id, ExpectedDeliveryDate, Last_Location__Latitude__s, Last_Location__Longitude__s, Status, Last_Integration_Update__c 
                                    FROM Shipment 
                                    WHERE Id = :shipmentId];

        System.assertEquals('Shipped', updatedShipment.Status);
        System.assertEquals(System.today().day(), updatedShipment.Last_Integration_Update__c.day());
        System.assertNotEquals(null, updatedShipment.ExpectedDeliveryDate);
        System.assertNotEquals(null, updatedShipment.Last_Location__Latitude__s);
        System.assertNotEquals(null, updatedShipment.Last_Location__Longitude__s);


    }
    @isTest
    static void testInvocable(){

        Shipment shipment = ShipmentTestDataFactory.createShipment();

        List<ShipmentServiceInvocable.InputParams> inputs = new List<ShipmentServiceInvocable.InputParams>();
        ShipmentServiceInvocable.InputParams param = new ShipmentServiceInvocable.InputParams();
        param.shipmentId = shipment.Id;
        param.carrier = 'fedex';
        param.trackingNumber = '123456';
        inputs.add(param);

        Test.setMock(HttpCalloutMock.class, new ShipmentTrackingHttpCalloutMock());
        Test.startTest();
        ShipmentServiceInvocable.getShipmentUpdate(inputs);
        Test.stopTest();

        Shipment updatedShipment = [SELECT Id, ExpectedDeliveryDate, Last_Location__Latitude__s, Last_Location__Longitude__s, Status, Last_Integration_Update__c 
                                    FROM Shipment 
                                    WHERE Id = :shipment.Id];

        System.assertEquals('Shipped', updatedShipment.Status);
        System.assertEquals(System.today().day(), updatedShipment.Last_Integration_Update__c.day());
        System.assertNotEquals(null, updatedShipment.ExpectedDeliveryDate);
        System.assertNotEquals(null, updatedShipment.Last_Location__Latitude__s);
        System.assertNotEquals(null, updatedShipment.Last_Location__Longitude__s);

    }

    /*@isTest
    static void testDelivered() {

        Test.setMock(HttpCalloutMock.class, new ShipmentTrackingHttpCalloutMock());
        Test.startTest();
        ShipmentService.makeCallout(shipment.Id,'fedex','123456');
        Test.stopTest();

        Shipment updatedShipment = [SELECT Id, ExpectedDeliveryDate, Last_Location__Latitude__s, Last_Location__Longitude__s, Status, Last_Integration_Update__c 
        FROM Shipment 
        WHERE Id = :shipment.Id];

        System.assertEquals('Delivered', updatedShipment.Status);
        System.assertEquals(System.today().day(), updatedShipment.Last_Integration_Update__c.day());
        System.assertNotEquals(null, updatedShipment.ExpectedDeliveryDate);
        System.assertNotEquals(null, updatedShipment.Last_Location__Latitude__s);
        System.assertNotEquals(null, updatedShipment.Last_Location__Longitude__s);

    }*/

}