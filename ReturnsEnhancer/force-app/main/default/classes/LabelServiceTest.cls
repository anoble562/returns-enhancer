/**
 * @File Name          : LabelServiceTest.cls
 * @Covers             : LabelService.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/
@isTest
public class LabelServiceTest {

    @isTest
    static void createLabelTest() {

        Shipment shipment = ShipmentTestDataFactory.createShipment();

        Test.setMock(HttpCalloutMock.class, new ShipmentTrackingHttpCalloutMock());
        Test.startTest();
        LabelService.createLabel(shipment.Id);
        Test.stopTest();

        Shipment updatedShipment = [SELECT Id, TrackingNumber, Status 
                                    FROM Shipment 
                                    WHERE Id = :shipment.Id];

        ContentDocumentLink cd = [SELECT Id, ContentDocument.LatestPublishedVersion.VersionData 
                                 FROM ContentDocumentLink 
                                 WHERE LinkedEntityId = :shipment.Id
                                 LIMIT 1];

        System.assertNotEquals(null, updatedShipment.TrackingNumber);
        System.assertNotEquals(null, cd);

    }
}