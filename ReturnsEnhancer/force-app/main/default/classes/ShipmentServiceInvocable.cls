/**
 * @File Name          : ShipmentServiceInvocable.cls
 * @Description        : This class is used to make tracking update callouts and related functionality.
 * @Covered By         : ShipmentServiceTest.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

public with sharing class ShipmentServiceInvocable {
    
    @InvocableMethod(label='Get Shipment Update')
    public static void getShipmentUpdate(List<InputParams> inputParams) {

    ShipmentService.makeCallout(inputParams[0].shipmentId, inputParams[0].carrier, inputParams[0].trackingNumber);

    }

    public class InputParams{
       
        @InvocableVariable(required=true)
        public String shipmentId;

        @InvocableVariable(required=true)
        public String carrier;

        @InvocableVariable(required=true)
        public String trackingNumber;

    }
}