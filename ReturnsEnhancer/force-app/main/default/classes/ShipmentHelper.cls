/**
 * @File Name          : ShipmentHelper.cls
 * @Description        : Helper class for ShipmentHandler
 * @Covered By         : TestClass.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

public without sharing class ShipmentHelper {

    public static void sendShipmentConfirmationEmail(Id shipmentId) {
        try {
            EmailTemplate template = [
                SELECT Id, Subject, HtmlValue
                FROM EmailTemplate
                WHERE DeveloperName = 'LA_Shipment_Confirmation_VF'
            ];
            Shipment shipment = [
                SELECT Id, Order__c, Order__r.ShipToContactId, Status
                FROM Shipment
                WHERE Id =: shipmentId
            ];
            if (shipment.Order__c != null) {

                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

                if (shipment != null && template != null && shipment.Order__c != null && shipment.Order__r.ShipToContactId != null) {
                    email.setTemplateId(template.Id);
                    email.setTargetObjectId(shipment.Order__r.ShipToContactId);
                    email.setWhatId(shipment.Id);
                    email.setTreatTargetObjectAsRecipient(true);
                }
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                    email
                });
            }
        } catch (Exception e) {
            LogService.insertLog(shipmentId, e);
        }
    }

    public static void sendDeliveryConfirmationEmail(Id shipmentId) {
        try {
            EmailTemplate template = [
                SELECT Id, Subject, HtmlValue
                FROM EmailTemplate
                WHERE DeveloperName = 'LA_Delivery_Confirmation_VF'
            ];
            Shipment shipment = [
                SELECT Id, Order__c, Order__r.ShipToContactId, Provider, TrackingNumber, Status
                FROM Shipment
                WHERE Id =: shipmentId
            ];
            if (shipment.Order__c != null) {

                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

                if (shipment != null && template != null && shipment.Order__c != null && shipment.Order__r.ShipToContactId != null) {
                    email.setTemplateId(template.Id);
                    email.setTargetObjectId(shipment.Order__r.ShipToContactId);
                    email.setWhatId(shipment.Id);
                    email.setTreatTargetObjectAsRecipient(true);
                }
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                    email
                });
                stopTrackingWebhook(shipment.Id, shipment.Provider, shipment.TrackingNumber);
            }
        } catch (Exception e) {
            LogService.insertLog(shipmentId, e);
        }
    }

    @future(callout = true)
    public static void stopTrackingWebhook(String shipmentId, String carrier, String tracking) {

        try {
            String carrierTxt = carrier;
            String trackingTxt = tracking;
            String url = 'https://api.shipengine.com/v1/tracking/stop?carrier_code=' + carrierTxt + '&tracking_number=' + trackingTxt;

            HttpResponse response = ShipmentCalloutUtility.makeCallout(url, 'POST', null);

            if (response.getStatusCode() == 200 || response.getStatusCode() == 204) {

                System.debug('Shipment ' + shipmentId + 'is no longer being tracked automatically');
            }

        } catch (Exception e) {
            LogService.insertLog(shipmentId, e);
        }
    }
}