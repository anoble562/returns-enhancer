/**
 * @File Name          : ShipmentTestDataFactory.cls
 * @Description        : This class is used create data for shipment related test classes
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

@isTest
public class ShipmentTestDataFactory {

    public static String contactId;

    public static Product2 createProduct(){
        Product2 product = new Product2();
        product.Name = 'Test Product';
        product.ProductCode = '1';
        insert product;
        return product;       
    }
    
    public static PricebookEntry createPricebookEntry(){
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = Test.getStandardPricebookId();
        pbe.Product2Id = createProduct().Id;
        pbe.UnitPrice = 100;
        insert pbe;
        return pbe;
    }

    public static Account createAccount(){

        Account acct = new Account();
        acct.Name = 'Test Account';

        acct.ShippingStreet = '1 Main Street';
        acct.ShippingCity = 'Testville';
        acct.ShippingState = 'CA';
        acct.ShippingCountry = 'US';
        acct.ShippingPostalCode = '10000';

        insert acct;
        Contact contact = createContact(acct.Id);

        return acct;
    }

    public static Contact createContact(Id acctId){
        Contact contact = new Contact();
        contact.FirstName = 'Test';
        contact.LastName = 'Person';
        contact.Email = 'test999990@test.com';
        contact.AccountId = acctId;
        insert contact;
        contactId = contact.Id;
        return contact;

    }

    public static Order createOrder(){
        
        Account a = createAccount();

        Order order = new Order();
        order.Status = 'Draft';
        order.EffectiveDate = Date.today();
        order.AccountId = a.Id;
        order.Pricebook2Id = Test.getStandardPricebookId();
        order.ShipToContactId = contactId;
        insert order;

        OrderItem orderLine = new OrderItem();
        orderLine.OrderId = order.Id;
        orderLine.Quantity = 1;
        orderLine.UnitPrice = 1;
        orderLine.PricebookEntryId = createPricebookEntry().Id;
        insert orderLine;

        return order;
    }

    public static Shipment createShipment() {
        Shipment shipment = new Shipment();
        shipment.TrackingNumber = '123456';
        shipment.Provider = 'fedex';
        shipment.Shipment_Type__c = 'fedex_ground';
        shipment.ShipToName = 'Recipient';
        shipment.Order__c = createOrder().Id;
        shipment.Generate_Label__c = true;
        shipment.Status = 'Pending Shipment';
        shipment.isAutoTracked__c = true;

        shipment.Package_Depth__c = 1;
        shipment.Package_Height__c = 1;
        shipment.Package_Weight__c = 1;
        shipment.Package_Width__c = 1;
        shipment.Measurement_Unit__c = 'inch';
        shipment.Weight_Unit__c = 'pound';

        shipment.ShipFromStreet = '625 Canyon Oaks Dr.';
        shipment.ShipFromCity = 'Oakland';
        shipment.ShipFromState = 'CA';
        shipment.ShipFromCountry = 'US';
        shipment.ShipFromPostalCode = '94605';

        shipment.ShipToStreet = '525 Canyon Oaks Dr.';
        shipment.ShipToCity = 'Oakland';
        shipment.ShipToState = 'CA';
        shipment.ShipToCountry = 'US';
        shipment.ShipToPostalCode = '94605';

        insert shipment;
        return shipment;
    }
}