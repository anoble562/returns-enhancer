public class ReturnsEnhancerOverviewMapController {
    
	@AuraEnabled
    public static list<shipmentAddressWrapper> getLocations(String returnOrderId, String filterSetting){

        List<shipmentAddressWrapper> shipmentWrapList = new list<shipmentAddressWrapper>();
        List<Shipment> shipmentList = new List<Shipment>();

        if(filterSetting == 'all'){
        shipmentList = [SELECT ShipmentNumber, Current_City__c, Current_Postal_Code__c, Current_Country__c, Current_State__c, Direction__c 
                                       FROM Shipment 
                                       WHERE Return_Order__c = :returnOrderId
                                      ];
        }else if(filterSetting == 'inbound'){
            shipmentList = [SELECT ShipmentNumber, Current_City__c, Current_Postal_Code__c, Current_Country__c, Current_State__c, Direction__c 
                                       FROM Shipment 
                                       WHERE Return_Order__c = :returnOrderId
                                       AND Direction__c = 'Inbound'
                                      ];

        }else if(filterSetting == 'outbound'){
            shipmentList = [SELECT ShipmentNumber, Current_City__c, Current_Postal_Code__c, Current_Country__c, Current_State__c, Direction__c 
                                       FROM Shipment 
                                       WHERE Return_Order__c = :returnOrderId
                                       AND Direction__c = 'Outbound'
                                      ];

        }

            for(Shipment ship : shipmentList){
        
            mapLocationWrapper mapList = new mapLocationWrapper();
            mapList.PostalCode = ship.Current_Postal_Code__c;
            mapList.City = ship.Current_City__c;
            mapList.State = ship.Current_State__c;
            maplist.country = ship.Current_Country__c;  
                     
            shipmentAddressWrapper shipWrap = new shipmentAddressWrapper();

            shipWrap.location = maplist;
            shipWrap.title = ship.ShipmentNumber;
 
                
            if(ship.Direction__c == 'Inbound'){
                  shipWrap.icon = 'action:recall';
            }else{
            shipWrap.icon = 'action:check';
            }
           	shipmentWrapList.add(shipWrap); 
        }
        return shipmentWrapList;
    }

    public class shipmentAddressWrapper{
        @AuraEnabled public string icon {get;set;} 
        @AuraEnabled public string title {get;set;}  
        @AuraEnabled public mapLocationWrapper location {get;set;} 
    }
    
    public class mapLocationWrapper{
        @AuraEnabled 
        public string Street{get;set;}
        @AuraEnabled 
        public string PostalCode{get;set;}
        @AuraEnabled 
        public string City{get;set;}
        @AuraEnabled 
        public string State{get;set;}
        @AuraEnabled 
        public string Country{get;set;}
    } 
}