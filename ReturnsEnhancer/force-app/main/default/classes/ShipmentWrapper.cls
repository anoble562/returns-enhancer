/**
 * @File Name          : ShipmentWrapper.cls
 * @Description        : Wrapper class for all Shipment-related functionality
 * @Covered By         : TestClass.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

public with sharing class ShipmentWrapper {

    public class WebhookListenerWrap{
    public String resource_url;
	public String resource_type;
	public TrackingRequest data;
    }

    public class TrackingRequest{
	public String tracking_number;
	public String status_code;
	public String status_description;
	public String carrier_status_code;
	public String carrier_status_description;
	public String ship_date;
	public Datetime estimated_delivery_date;
	public Datetime actual_delivery_date;
	public String exception_description;
    public List<EventsWrapper> events;
    }

	public class EventsWrapper {
		public String occurred_at;
		public String carrier_occurred_at;
		public String description;
		public String city_locality;
		public String state_province;
		public String postal_code;
		public String country_code;
		public String company_name;
		public String signer;
		public String event_code;
		public String latitude;
		public String longitude;
    }

    public class LabelRequestWrapper{
        public Shipment shipment;
    }
    
    public class Shipment {
        public String service_code;
        public Ship_to ship_to;
        public Ship_from ship_from;
        public List<Packages> packages;
    }

	public class Packages {
		public Dimensions dimensions;
		public Weight weight;
	}

	public class Ship_To {
		public String name;
		public String address_line1;
		public String city_locality;
		public String state_province;
		public String postal_code;
		public String country_code;
		public String address_residential_indicator;
	}

	public class Dimensions {
		public Decimal height;
		public Decimal width;
		public Decimal length;
		public String unit;
	}

	public class Weight {
		public Decimal value;
		public String unit;
	}

	public class Ship_From {
		public String name;
		public String company_name;
		public String phone;
		public String address_line1;
		public String city_locality;
		public String state_province;
		public String postal_code;
		public String country_code;
		public String address_residential_indicator;
    }

    public class LabelResponseWrapper {
        public String label_id;
        public String status;
        public String shipment_id;
        public Datetime ship_date;
        public Datetime created_at;
        public Shipment_cost shipment_cost;
        public Shipment_cost insurance_cost;
        public String charge_event;
        public String tracking_number;
        public Boolean is_return_label;
        public String rma_number;
        public Boolean is_international;
        public String batch_id;
        public String carrier_id;
        public String service_code;
        public String package_code;
        public Boolean voided;
        public String voided_at;
        public String label_format;
        public String label_layout;
        public Boolean trackable;
        public String label_image_id;
        public String carrier_code;
        public String tracking_status;
        public Label_download label_download;
        public String form_download;
        public String insurance_claim;
        public List<ResponsePackages> packages;
    }
        public class ResponsePackages {
            public String package_code;
            public ResponseWeight weight;
            public ResponseDimensions dimensions;
            public Shipment_cost insured_value;
            public String tracking_number;
            public Label_messages label_messages;
            public String external_package_id;
        }
    
        public class Label_download {
            public String pdf;
            public String png;
            public String zpl;
            public String href;
        }
    
        public class Label_messages {
            public String reference1;
            public String reference2;
            public String reference3;
        }
    
        public class ResponseDimensions {
            public String unit;
            public Decimal length;
            public Decimal width;
            public Decimal height;
        }
    
        public class Shipment_cost {
            public String currency_Z;
            public Decimal amount;
        }
    
        public class ResponseWeight {
            public Decimal value;
            public String unit;
        }

        public class AddressValidationRequest{
        public List<Address> addresses;
        }

        public class Address{
            public String address_line1;
            public String city_locality;
            public String state_province;
            public String postal_code;
            public String country_code;
	    }  
        
        public class AddressValidationResponse {
            public String status;
            public OriginalAddress original_address;
            public MatchedAddress matched_address;
            public Messages[] messages;
        }

        public class OriginalAddress {
            public String name;
            public String phone;
            public String company_name;
            public String address_line1;
            public String address_line2;
            public String address_line3;
            public String city_locality;
            public String state_province;
            public String postal_code;
            public String country_code;
            public String address_residential_indicator;
        }

        public class MatchedAddress {
            public String name;
            public String phone;
            public String company_name;
            public String address_line1;	
            public String address_line2;	
            public String address_line3;
            public String city_locality;
            public String state_province;
            public String postal_code;
            public String country_code;
            public String address_residential_indicator;
        }

        public class Messages {
        }
}