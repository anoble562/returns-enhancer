/**
 * @File Name          : ShipmentService.cls
 * @Description        : This class is used to make tracking update callouts and related functionality.
 * @Covered By         : ShipmentServiceTest.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

public with sharing class ShipmentService {

        @future(callout=true)
        public static void makeCallout(String shipmentId, String carrier, String tracking){
        
        try{
            Shipment shipment = new Shipment();
            String carrierTxt = carrier;
            String trackingTxt = tracking;
            String body = '';
            String url = 'https://api.shipengine.com/v1/tracking?carrier_code=' + carrierTxt + '&tracking_number=' + trackingTxt;

            HttpResponse response = ShipmentCalloutUtility.makeCallout(url, 'GET', body);

            if (response.getStatusCode() == 200 && response.getBody() != null) {

                ShipmentWrapper.TrackingRequest wrap = (ShipmentWrapper.TrackingRequest)System.JSON.deserialize(response.getBody(), ShipmentWrapper.TrackingRequest.class);
                System.debug('Updates: '+wrap);

                shipment.Id = shipmentId;

                if(wrap.actual_delivery_date != null){
                    String actual_deliv = wrap.actual_delivery_date.formatGMT('yyyy-MM-dd HH:mm:ss');
                    shipment.ActualDeliveryDate = Datetime.valueof(actual_deliv);
                }
                if(wrap.estimated_delivery_date != null){
                    String est_deliv = wrap.estimated_delivery_date.formatGMT('yyyy-MM-dd HH:mm:ss');
                    shipment.ExpectedDeliveryDate = Datetime.valueof(est_deliv);
                }
                
                shipment.Shipment_Details__c = wrap.carrier_status_description;
                System.debug('Events --- '+wrap.events);

                if(!wrap.events.isEmpty()){
                    
                    if(wrap.events[0].latitude != null && wrap.events[0].longitude != null){
                    shipment.Last_Location__Latitude__s = Decimal.valueOf(wrap.events[0].latitude);
                    shipment.Last_Location__Longitude__s = Decimal.valueOf(wrap.events[0].longitude);
                    }
                    shipment.Current_Postal_Code__c = wrap.events[0].postal_code;
                    shipment.Current_City__c = wrap.events[0].city_locality;
                    shipment.Current_State__c = wrap.events[0].state_province;
                    shipment.Current_Country__c = wrap.events[0].country_code;
                    shipment.Signor__c = wrap.events[0].signer;
                    
                    }else{
                    shipment.Status = 'Unknown';
                    }

                if(wrap.status_description.contains('Delivered')){
                    shipment.Status = 'Delivered';
  
                    String webhookUrl = 'https://api.shipengine.com/v1/tracking/stop?carrier_code=' + carrierTxt + '&tracking_number=' + trackingTxt;

                    HttpResponse webhookResponse = ShipmentCalloutUtility.makeCallout(webhookUrl, 'POST', null);

                    if (webhookResponse.getStatusCode() == 200 || webhookResponse.getStatusCode() == 204) {
                        System.debug('Shipment '+shipmentId+'is no longer being tracked automatically');
                    }

                }else if(wrap.status_description == 'In Transit' || wrap.status_description == 'Accepted'){
                    shipment.Status = 'Shipped';

                }else{
                    shipment.Status = 'Unknown';
                }

            shipment.Last_Integration_Update__c = Datetime.now();

            }
            
            update shipment;

        }catch(Exception e){
            LogService.insertLog(shipmentId, e);
        } 
    }
}