public with sharing class ReturnLabelEmailService {

        public static void sendEmail(String shipmentId){
            try{
                if(shipmentId != null){

                    Shipment shipment = [SELECT Id, Contact__c FROM Shipment WHERE Id = :shipmentId];
                    RMA_Enhancer_Setting__mdt settings = [SELECT Return_Label_Email_Template__c FROM RMA_Enhancer_Setting__mdt WHERE MasterLabel = 'Default'];
    
                    EmailTemplate template = [SELECT Id, Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = :settings.Return_Label_Email_Template__c];
                    //OrgWideEmailAddress orgWideEmail = [SELECT Id FROM OrgWideEmailAddress WHERE Address = '<<ADDRESS_HERE>>'];
    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateId(template.Id);
                    mail.setTargetObjectId(shipment.Contact__c);
                    mail.setWhatId(shipment.Id); 
                    mail.setTreatTargetObjectAsRecipient(true);
                    
                    List<Messaging.EmailFileAttachment> fileAttachments = getAttachments(shipment);
                    
                    if(!fileAttachments.isEmpty()){
                        mail.setFileAttachments(fileAttachments);
                    }
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
                }
            } catch (Exception ex) {
                //<<ERROR_LOGGING>>
            }
        }
    
        private static List<Messaging.EmailFileAttachment> getAttachments(Shipment shipment){
            List<Messaging.EmailFileAttachment> fileAttachments = new List<Messaging.EmailFileAttachment>(); 
            if(shipment != null){

                ContentDocumentLink cdl = [SELECT Id, ContentDocumentId
                                           FROM ContentDocumentLink
                                           WHERE LinkedEntityId = :shipment.Id
                                           LIMIT 1];
    
                for (ContentVersion cVersion : [SELECT Id, ContentDocument.Title, VersionData, FileType 
                                                FROM ContentVersion 
                                                WHERE ContentDocumentId =: cdl.ContentDocumentId
                                                ORDER BY CreatedDate DESC LIMIT 1]){
                    Blob attachmentBody = cVersion.VersionData;
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                    efa.setFileName(cVersion.ContentDocument.Title+'.'+cversion.FileType);
                    efa.setBody(attachmentBody);    
                    fileAttachments.add(efa);
                }    
            }
            return fileAttachments;
        }
}