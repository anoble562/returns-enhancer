public without sharing class DepotWorkOrderGenerationUtility {
    
    public static void createWorkOrder(String shipmentId) {

        try{
            if(shipmentId != null){

                List<WorkOrder> woToInsert = new List<WorkOrder>();
                WorkOrder wo = new WorkOrder();

                Shipment shipment = [SELECT Id, Contact__c, Return_Order__c, Return_Order__r.AccountId, Return_Order__r.CaseId, DestinationLocationId FROM Shipment WHERE Id = :shipmentId];
                RMA_Enhancer_Setting__mdt settings = [SELECT Autogenerate_Work_Order_on_Delivery__c, Depot_Work_Type_Name__c FROM RMA_Enhancer_Setting__mdt WHERE MasterLabel = 'Default'];

                if(settings != null){
                    WorkType workType = [SELECT Id FROM WorkType WHERE Name = :settings.Depot_Work_Type_Name__c LIMIT 1];
                
                    if(settings.Autogenerate_Work_Order_on_Delivery__c == true){

                        wo.ContactId = shipment.Contact__c;
                        wo.AccountId = shipment.Return_Order__r?.AccountId;
                        wo.CaseId = shipment.Return_Order__r?.CaseId;
                        wo.Return_Order__c = shipment.Return_Order__c;
                        wo.LocationId = shipment.DestinationLocationId;
                        wo.WorkTypeId = workType.Id;
                        wo.Shipment__c = shipment.Id;
                        woToInsert.add(wo);
                    }
                }
                if(!woToInsert.isEmpty()){
                    insert woToInsert;
                }

                List<WorkOrderLineItem> wolisToInsert = createWOLIs(wo, shipment);
                if(!wolisToInsert.isEmpty()){
                    System.debug(wolisToInsert);
                    insert wolisToInsert;
                }
            }
        }catch(Exception e){
            //Exception handling here
            System.debug('Error: '+e);
            LogService.insertLog(shipmentId, e);
        }
    }

    public static List<WorkOrderLineItem> createWOLIs(WorkOrder wo, Shipment shipment){

        List<WorkOrderLineItem> wolisToReturn = new List<WorkOrderLineItem>();

        if(wo != null && shipment != null){

            List<ReturnOrderLineItem> roliList = [
                SELECT AssetId 
                FROM ReturnOrderLineItem
                WHERE ReturnOrderId = :shipment.Return_Order__c];

            if(!roliList.isEmpty()){
                for(ReturnOrderLineItem roli : roliList){
                    WorkOrderLineItem woli = new WorkOrderLineItem();
                    woli.WorkOrderId = wo.Id;
                    woli.AssetId = roli.AssetId;
                    woli.ReturnOrderLineItemId = roli.Id;
                    wolisToReturn.add(woli);

                }
            }
        }
        return wolisToReturn;
    }
}