/**
 * @File Name          : LogService.cls
 * @Description        : Used to capture error logs when exceptions occur
 * @Covered By         : TestClass.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

public with sharing class LogService {

    public static void insertLog (Id objectId, Exception e) {

        Error_Logs__c log = new Error_Logs__c();
        log.Object_Id__c = objectId;
        log.Error_Message__c = e.getMessage();
        log.Stack_Trace__c = e.getStackTraceString();
        log.Occurred_At__c = Datetime.now();
        insert log;

    }
}