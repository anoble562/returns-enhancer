public class printLabelController {

    @AuraEnabled
    public static List<ContentVersion> getContents(Id shipmentId) {
        Id contentDocId;
        System.debug('Alextest '+ shipmentId);

        for (ContentDocumentLink cdl : [SELECT Id, ContentDocumentId 
                                        FROM ContentDocumentLink 
                                        WHERE LinkedEntityId = :shipmentId]) {
            contentDocId = cdl.ContentDocumentId;
        }

        return [SELECT Id,Title FROM ContentVersion WHERE ContentDocumentId = :contentDocId ORDER BY CreatedDate DESC LIMIT 1];
    }
}