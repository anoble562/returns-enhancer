@isTest
public class ShipmentHelperTest {

    /*@isTest
    static void addressTest() {

        Shipment shipment = ShipmentTestDataFactory.createShipment();

        Test.startTest();
        ShipmentHelper.populateAddresses(shipment.Id, shipment.ShipToAddress, shipment.ShipFromAddress);
        Test.stopTest();

        Shipment updatedShipment = [SELECT ShipToStreet, ShipFromStreet FROM Shipment WHERE Id = :shipment.Id];

        System.assertEquals('1 Main Street', updatedShipment.ShipToStreet);
        System.assertEquals('2 Main Street', updatedShipment.ShipFromStreet);

    }*/

    @isTest
    static void shipmentEmailTest(){

        ShipmentHandler.TriggerDisabled = true;
        Shipment shipment = ShipmentTestDataFactory.createShipment();

        Test.startTest();
        shipment.Status = 'Shipped';
        ShipmentHelper.sendShipmentConfirmationEmail(shipment.Id);
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();

        System.assertEquals(1, invocations);
    }

    @isTest
    static void deliveredEmailTest(){

        ShipmentHandler.TriggerDisabled = true;
        Shipment shipment = ShipmentTestDataFactory.createShipment();

        Test.setMock(HttpCalloutMock.class, new ShipmentTrackingHttpCalloutMock());

        Test.startTest();
        shipment.Status = 'Delivered';
        ShipmentHelper.sendDeliveryConfirmationEmail(shipment.Id);
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();

        System.assertEquals(1, invocations);
    }
}