/**
 * @File Name          : LabelService.cls
 * @Description        : This class is used to generate shipping labels.
 * @Covered By         : LabelServiceTest.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

public with sharing class ReturnLabelService {

    public static void createLabel(String shipmentId){

        try{
        
            Shipment shipment = [SELECT Package_Height__c, Package_Width__c, Package_Depth__c, Package_Weight__c, Weight_Unit__c, Measurement_Unit__c, Provider, Shipment_Type__c, Generate_Label__c,
                                        ShipFromStreet, ShipFromCity, ShipFromState, ShipFromCountry, ShipFromPostalCode,
                                        ShipToStreet, ShipToCity, ShipToState, ShipToCountry, ShipToPostalCode, ShipToName,
                                        Return_Order__r.Contact.Name
                                        FROM Shipment 
                                        WHERE Id = :shipmentId];

            Organization org = [SELECT o.Name, o.Phone FROM Organization o];

            if(shipment != null){   

                if(shipment.Generate_Label__c = true){

                //Structure wrapper for JSON request
                ShipmentWrapper.Dimensions dims = new ShipmentWrapper.Dimensions();
                dims.height=shipment.Package_Height__c;
                dims.width=shipment.Package_Width__c;
                dims.length=shipment.Package_Depth__c;
                dims.unit=shipment.Measurement_Unit__c;

                ShipmentWrapper.Weight wt = new ShipmentWrapper.Weight();
                wt.value=shipment.Package_Weight__c;
                wt.unit=shipment.Weight_Unit__c;

                List<ShipmentWrapper.Packages> pkgList = new List<ShipmentWrapper.Packages>();
                ShipmentWrapper.Packages pkg = new ShipmentWrapper.Packages();
                pkg.dimensions=dims;
                pkg.weight=wt;
                pkgList.add(pkg);

                ShipmentWrapper.Ship_To ship_to = new ShipmentWrapper.Ship_To();
                ship_to.name = shipment.ShipToName;
                ship_to.address_line1 = shipment.ShipToStreet;
                ship_to.city_locality = shipment.ShipToCity;
                ship_to.state_province = shipment.ShipToState;
                ship_to.postal_code = shipment.ShipToPostalCode;
                ship_to.country_code = shipment.ShipToCountry;
                ship_to.address_residential_indicator = 'no';
            
                ShipmentWrapper.Ship_From ship_from = new ShipmentWrapper.Ship_From();
                ship_from.name = shipment.Return_Order__r?.Contact?.Name;
                ship_from.company_name = org.Name;
                ship_from.phone = org.Phone;
                ship_from.address_line1 = shipment.ShipFromStreet;
                ship_from.city_locality = shipment.ShipFromCity;
                ship_from.state_province = shipment.ShipFromState;
                ship_from.postal_code = shipment.ShipFromPostalCode;
                ship_from.country_code = shipment.ShipFromCountry;
                ship_from.address_residential_indicator = 'no';
            
                ShipmentWrapper.Shipment shpmnt = new ShipmentWrapper.Shipment();
                shpmnt.service_code = (shipment.Shipment_Type__c).toLowercase();
                shpmnt.ship_to = ship_to;
                shpmnt.ship_from = ship_from;
                shpmnt.packages = pkgList;

                ShipmentWrapper.LabelRequestWrapper labelWrapper = new ShipmentWrapper.LabelRequestWrapper();
                labelWrapper.shipment = shpmnt;
            
                String jsonRequest = System.JSON.serialize(labelWrapper);
                LabelService.getLabel(shipmentId, jsonRequest, shipment.Provider);
                }
            }     
        }catch(Exception e){
            LogService.insertLog(shipmentId, e);
        }
    }

    @future(callout=true)
    public static void getLabel(String shipmentId, String jsonRequest, String carrier){
        try{

        Shipment shipmnt = [SELECT Id,TrackingNumber,isAutoTracked__c 
                            FROM Shipment 
                            WHERE Id = :shipmentId];

        String url = 'https://api.shipengine.com/v1/labels';
        HttpResponse response = ShipmentCalloutUtility.makeCallout(url,'POST',jsonRequest);
    
        if(response.getStatusCode() == 200 && response.getBody() != null) {
            
            ShipmentWrapper.LabelResponseWrapper wrap = (ShipmentWrapper.LabelResponseWrapper)System.JSON.deserialize(response.getBody(), ShipmentWrapper.LabelResponseWrapper.class);
 
            shipmnt.TrackingNumber = wrap.tracking_number;

            if(wrap.shipment_cost != null){
                ShipmentWrapper.Shipment_cost shipCost = wrap.shipment_cost;
                ShipmentWrapper.Shipment_cost insuranceCost = wrap.insurance_cost;

                shipmnt.Shipping_Cost__c = shipCost.amount;
                shipmnt.Insurance_Cost__c = insuranceCost.amount;
            }

            if(shipmnt.isAutoTracked__c){
                String carrierTxt = carrier;
                String trackingTxt = shipmnt.TrackingNumber;
                String body ='';
                String webhookURL = 'https://api.shipengine.com/v1/tracking/start?carrier_code=' + carrierTxt + '&tracking_number=' + trackingTxt;
    
                HttpResponse webhookResponse = ShipmentCalloutUtility.makeCallout(webhookURL, 'POST', body);
    
                if (webhookResponse.getStatusCode() == 200 || webhookResponse.getStatusCode() == 204) {
                    System.debug('Shipment '+shipmentId+'is now being tracked automatically');
                }  
            }   

            String pdfUrl = wrap.label_download.pdf;

            PageReference pageRef = new PageReference(pdfUrl);
            Blob pdf;

            if(Test.isRunningTest()){
                pdf = blob.valueOf('test');
            }else{
                pdf = pageRef.getContentAsPDF();
            }
            
            ContentVersion ContVerFile = new ContentVersion();
            ContVerFile.VersionData = pdf;
            ContVerFile.Title = 'Label: '+wrap.tracking_number; 
            ContVerFile.ContentLocation = 's';
            ContVerFile.PathOnClient = wrap.tracking_number+'.pdf';
            insert ContVerFile;
        
            Id conDoc = [SELECT ContentDocumentId 
                        FROM ContentVersion 
                        WHERE Id =:ContVerFile.Id].ContentDocumentId;

            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDoc;
            cDe.LinkedEntityId = shipmentId;
            cDe.ShareType = 'I';
            cDe.Visibility = 'AllUsers';
            insert cDe;
                      
            update shipmnt;

            }else{
                System.debug('Error Occured. Response: '+response.getBody());
            }
        }catch(Exception e){
            LogService.insertLog(shipmentId, e);
        }
    }
}