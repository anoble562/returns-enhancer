public class ShipmentOverviewMapApexController { 

    @AuraEnabled
    public static list<shipmentAddressWrapper> getLocation(){

        List<shipmentAddressWrapper> shipmentWrapList = new list<shipmentAddressWrapper>();

        List<Shipment> shipmentList = [SELECT ShipmentNumber, Current_City__c, Current_Postal_Code__c, Current_Country__c, Current_State__c 
                                        FROM Shipment 
                                        WHERE Status = 'Unknown'];

            for(Shipment ship : shipmentList){
        
            mapLocationWrapper mapList = new mapLocationWrapper();
            mapList.PostalCode = ship.Current_Postal_Code__c;
            mapList.City = ship.Current_City__c;
            mapList.State = ship.Current_State__c;
            maplist.country = ship.Current_Country__c;  
                     
            shipmentAddressWrapper accWrap = new shipmentAddressWrapper();
            accWrap.icon = 'utility:location';
            accWrap.location = maplist;
            accWrap.title = ship.ShipmentNumber;
            shipmentWrapList.add(accWrap);            
        }
        return shipmentWrapList;
    }

    public class shipmentAddressWrapper{
        @AuraEnabled public string icon {get;set;} 
        @AuraEnabled public string title {get;set;}  
        @AuraEnabled public mapLocationWrapper location {get;set;} 
    }
    
    public class mapLocationWrapper{
        @AuraEnabled 
        public string Street{get;set;}
        @AuraEnabled 
        public string PostalCode{get;set;}
        @AuraEnabled 
        public string City{get;set;}
        @AuraEnabled 
        public string State{get;set;}
        @AuraEnabled 
        public string Country{get;set;}
    } 
}