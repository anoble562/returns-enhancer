public class ReturnEnhancerDatatableApexController {

    @AuraEnabled
    public static List<ReturnOrderLineItem> fetchRoliLines(String returnOrderId) {

        List<ReturnOrderLineItem> roliList = [SELECT ReturnOrderLineItemNumber, Asset.Name, AssetId, QuantityReturned
                                              FROM ReturnOrderLineItem 
                                              WHERE ReturnOrderId = :returnOrderId];
        return roliList;
    }
}