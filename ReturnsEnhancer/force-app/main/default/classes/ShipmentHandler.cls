/**
 * @File Name          : ShipmentHandler.cls
 * @Description        : Handler class for Shipment Trigger
 * @Covered By         : TestClass.cls
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

public with sharing class ShipmentHandler implements IShipmentTriggerHandler {

    public static Boolean TriggerDisabled = false;

    public Boolean IsDisabled() {
        if (LogisticsAccelerator__c.getInstance(UserInfo.getProfileId()).Disable_Trigger__c == true 
            && Test.isRunningTest() == false) {
            return true;
        } else {
            return TriggerDisabled;
        }
    }

    public void BeforeInsert(List <SObject> newItems) {
        for (Shipment shipment: (List <Shipment> ) newItems) {
            System.debug(shipment);
        }
    }

    public void BeforeUpdate(Map < Id, SObject> newItems, Map <Id, SObject> oldItems) {}

    public void BeforeDelete(Map <Id, SObject> oldItems) {}

    public void AfterInsert(Map <Id, SObject> newItems) {
       /* for (SObject shipment: newItems.values()) {
            LabelService.createLabel(shipment.Id);
        }*/
    }

    public void AfterUpdate(Map <Id, SObject> newItems, Map <Id, SObject> oldItems) {
        for (Shipment newShipment: (List <Shipment> ) newItems.values()) {
            if (newShipment.Status == 'Shipped' && newShipment.Direction__c == 'Outbound') {
                for (Shipment oldShipment: (List <Shipment> ) oldItems.values()) {
                    if (oldShipment.Status != 'Shipped') {
                        ShipmentHelper.sendShipmentConfirmationEmail(newShipment.Id);
                    }
                }
            }
        }
        for (Shipment newShipment: (List <Shipment> ) newItems.values()) {
            if (newShipment.Status == 'Delivered' && newShipment.Direction__c == 'Outbound') {
                for (Shipment oldShipment: (List <Shipment> ) oldItems.values()) {
                    if (oldShipment.Status != 'Delivered') {
                        ShipmentHelper.sendDeliveryConfirmationEmail(newShipment.Id);
                    }
                }
            }
        }
        for (Shipment newShipment: (List <Shipment> ) newItems.values()) {
            if (newShipment.Status == 'Delivered' && newShipment.Direction__c == 'Inbound') {
                for (Shipment oldShipment: (List <Shipment> ) oldItems.values()) {
                    if (oldShipment.Status != 'Delivered') {
                        DepotWorkOrderGenerationUtility.createWorkOrder(newShipment.Id);
                    }
                }
            }
        }
    }

    public void AfterDelete(Map <Id, SObject> oldItems) {}

    public void AfterUndelete(Map <Id, SObject> oldItems) {}
}