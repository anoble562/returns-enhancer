public with sharing class ReturnEnhancerAddShipmentApexController {

    @AuraEnabled
    public static void createNewShipment(Shipment shipmentObj, String returnOrderId, String street, String city, String state, String country, String postalCode, List < ReturnOrderLineItem > roliList) {

        System.debug('Shipment obj is: ' + shipmentObj);
        System.debug('ROLIs in apex controller are: ' + roliList);

        try {

            List < ShipmentItem > sliList = new List < ShipmentItem > ();

            ReturnOrder ro = [SELECT DestinationLocationId, DestinationLocation.Shipping_Address__r.Street,
                DestinationLocation.Shipping_Address__r.City, DestinationLocation.Shipping_Address__r.State,
                DestinationLocation.Shipping_Address__r.Country, DestinationLocation.Shipping_Address__r.PostalCode
                FROM ReturnOrder
                WHERE Id =: returnOrderId
            ];

            if (shipmentObj != null && ro != null) {
                shipmentObj.Direction__c = 'Inbound';
                shipmentObj.ShipToName = 'Operations';
                shipmentObj.DestinationLocationId = ro.DestinationLocationId;
                shipmentObj.ShipToStreet = ro.DestinationLocation.Shipping_Address__r.Street;
                shipmentObj.ShipToCity = ro.DestinationLocation.Shipping_Address__r.City;
                shipmentObj.ShipToState = ro.DestinationLocation.Shipping_Address__r.State;
                shipmentObj.ShipToCountry = ro.DestinationLocation.Shipping_Address__r.Country;
                shipmentObj.ShipToPostalCode = ro.DestinationLocation.Shipping_Address__r.PostalCode;
                shipmentObj.Return_Order__c = ro.Id;

                shipmentObj.ShipFromStreet = street;
                shipmentObj.ShipFromCity = city;
                shipmentObj.ShipFromState = state;
                shipmentObj.ShipFromCountry = country;
                shipmentObj.ShipFromPostalCode = postalCode;

                shipmentObj.Current_City__c = city;
                shipmentObj.Current_State__c = state;
                shipmentObj.Current_Country__c = country;
                shipmentObj.Current_Postal_Code__c = postalCode;

                shipmentObj.Generate_Label__c = true;
                shipmentObj.isAutoTracked__c = true;

                insert shipmentObj;

                if (!roliList.isEmpty()) {
                    for (ReturnOrderLineItem roli: roliList) {
                        ShipmentItem sli = new ShipmentItem();
                        sli.ShipmentId = shipmentObj.Id;
                        sli.Asset__c = roli.AssetId;
                        sli.Quantity = roli.QuantityReturned;
                        sliList.add(sli);
                        System.debug('SLI list is: ' + sliList);
                    }
                }

                if (!sliList.isEmpty()) {
                    System.debug('SLIs inserted: ' + sliList);
                    insert sliList;
                }
            }
            ReturnLabelService.createLabel(shipmentObj.Id);
        } catch (Exception e) {
            LogService.insertLog(shipmentObj.Id, e);
        }
    }

    public class AddressWrapper {

        @AuraEnabled
        public String street {
            get;
            set;
        }
        @AuraEnabled
        public String city {
            get;
            set;
        }
        @AuraEnabled
        public String state {
            get;
            set;
        }
        @AuraEnabled
        public String country {
            get;
            set;
        }
        @AuraEnabled
        public String postalCode {
            get;
            set;
        }
    }

    @AuraEnabled
    public static Package_Template__c getPackageTemplateObject(String packageTemplateId) {

        Package_Template__c pt = new Package_Template__c();
        try {
            pt = [
                SELECT Carrier__c, Dimensions_Unit__c, Package_Depth__c, Package_Height__c, Package_Type__c,
                Package_Weight__c, Package_Width__c, Shipment_Type__c, Weight_Unit__c
                FROM Package_Template__c
                WHERE Id =: packageTemplateId
                LIMIT 1
            ];
         
        } catch(Exception e) {
            LogService.insertLog(packageTemplateId, e);
        }

        return pt;
    }
}