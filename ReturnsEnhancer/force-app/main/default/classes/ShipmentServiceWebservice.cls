global class ShipmentServiceWebservice {
    
    webservice static void callout(String shipmentId, String carrier, String tracking) {
        ShipmentService.makeCallout(shipmentId, carrier, tracking);
    }
}