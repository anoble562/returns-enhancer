public without sharing class ShipmentMapController {

    @AuraEnabled
    public static Coordinates getCoords(Id shipmentId){

        Shipment shipment = [SELECT Last_Location__Latitude__s,Last_Location__Longitude__s,
                                    ShipFromStreet,ShipFromCity,ShipFromState,ShipFromCountry,ShipFromPostalCode,
                                    Current_City__c,Current_Postal_Code__c,Current_State__c,
                                    SourceLocation.Shipping_Address__r.Latitude, SourceLocation.Shipping_Address__r.Longitude, SourceLocation.Name
                            FROM Shipment
                            WHERE Id = :shipmentId];

        Coordinates coords = new Coordinates();
      

        if(shipment.Last_Location__Latitude__s == null || shipment.Last_Location__Longitude__s == null){
            coords.Location = shipment.SourceLocation.Name;
            coords.Latitude = shipment.SourceLocation.Shipping_Address__r.Latitude;
            coords.Longitude = shipment.SourceLocation.Shipping_Address__r.Longitude;
        }else if(shipment.Current_City__c != null && shipment.Current_State__c != null && shipment.Current_Postal_Code__c != null){       
            coords.Location = shipment.Current_City__c+', '+shipment.Current_State__c+' '+shipment.Current_Postal_Code__c;
            coords.Latitude = shipment.Last_Location__Latitude__s;
            coords.Longitude = shipment.Last_Location__Longitude__s;
        }else{
            coords.Location = 'This is the shipment\'s last known location. Current location details not available';
            coords.Latitude = shipment.Last_Location__Latitude__s;
            coords.Longitude = shipment.Last_Location__Longitude__s;
        }

        return coords;
    }

    public class Coordinates{
        @AuraEnabled 
        public Decimal Latitude {get;set;} 
        @AuraEnabled 
        public Decimal Longitude {get;set;}
        @AuraEnabled 
        public String Location {get;set;}
    }
}