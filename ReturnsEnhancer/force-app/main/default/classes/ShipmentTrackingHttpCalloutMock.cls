/**
 * @File Name          : ShipmentTrackingHttpCalloutMock.cls
 * @Description        : Mock callout for shipment related classes.
 * @Author             : Alex Noble
 * @Created By         : NeuraFlash, LLC
 * @Modification Log   :
 *======================================================================================================================
 * Ver         Date                     Author                    Modification
 *======================================================================================================================
 * 1.0    	2/1/2021      				Alex Noble             	Initial Version

**/

@isTest
public class ShipmentTrackingHttpCalloutMock implements HttpCalloutMock {

    public HttpResponse respond (HttpRequest request) {

        HttpResponse response = new HttpResponse();

       if(request.getEndpoint().startsWith('https://api.shipengine.com/v1/tracking?')){
            //System.assertEquals('GET', request.getMethod());

            String body = '{\"tracking_number\":\"123456\",\"status_code\":\"IT\",\"status_description\":\"In Transit\",\"carrier_status_code\":\"01\",\"carrier_status_description\":\"Your item is in transit\",\"ship_date\":\"2019-07-26T22:10:50.286Z\",\"estimated_delivery_date\":\"2019-07-28T22:10:50.286Z\",\"actual_delivery_date\":\"2019-07-26T22:10:50.286Z\",\"exception_description\":null,\"events\":[{\"occurred_at\":\"2019-09-13T12:32:00Z\",\"carrier_occurred_at\":\"2019-09-13T05:32:00\",\"description\":\"Arrived at USPS Facility\",\"city_locality\":\"Oakland\",\"state_province\":\"CA\",\"postal_code\":\"94605\",\"country_code\":\"\",\"company_name\":\"\",\"signer\":\"\",\"event_code\":\"U1\",\"latitude\":\"37.801239\",\"longitude\":\"-122.258301\"}]}';
         
            response.setHeader('Content-Type', 'application/json');
            response.setBody(body);
            response.setStatusCode(200);
            return response;
        }

       else if(request.getEndpoint().startsWith('https://api.shipengine.com/v1/tracking/')){
            //System.assertEquals('POST', request.getMethod());

            response.setHeader('Content-Type', 'application/json');
            response.setBody('');
            response.setStatusCode(200);
            return response;

        }

        else if (request.getEndpoint().startsWith('https://api.shipengine.com/v1/labels')){
            //System.assertEquals('POST', request.getMethod());

            String body = '{\"label_id\":\"se-7764944\",\"status\":\"completed\",\"shipment_id\":\"se-21748537\",\"ship_date\":\"2020-04-17T00:00:00Z\",\"created_at\":\"2020-04-17T16:22:20.9879673Z\",\"shipment_cost\":{\"currency\":\"usd\",\"amount\":10.19},\"insurance_cost\":{\"currency\":\"usd\",\"amount\":0},\"charge_event\":\"carrier_default\",\"tracking_number\":\"123456\",\"is_return_label\":false,\"rma_number\":null,\"is_international\":false,\"batch_id\":\"\",\"carrier_id\":\"se-169349\",\"service_code\":\"fedex_ground\",\"package_code\":\"package\",\"voided\":false,\"voided_at\":null,\"label_format\":\"pdf\",\"label_layout\":\"4x6\",\"trackable\":true,\"label_image_id\":null,\"carrier_code\":\"fedex\",\"tracking_status\":\"in_transit\",\"label_download\":{\"pdf\":\"https://api.shipengine.com/v1/downloads/10/XNGDhq7uZ0CAEt5LOnCxIg/label-7764944.pdf\",\"png\":\"https://api.shipengine.com/v1/downloads/10/XNGDhq7uZ0CAEt5LOnCxIg/label-7764944.png\",\"zpl\":\"https://api.shipengine.com/v1/downloads/10/XNGDhq7uZ0CAEt5LOnCxIg/label-7764944.zpl\",\"href\":\"https://api.shipengine.com/v1/downloads/10/XNGDhq7uZ0CAEt5LOnCxIg/label-7764944.pdf\"},\"form_download\":null,\"insurance_claim\":null,\"packages\":[{\"package_code\":\"package\",\"weight\":{\"value\":20,\"unit\":\"ounce\"},\"dimensions\":{\"unit\":\"inch\",\"length\":0,\"width\":0,\"height\":0},\"insured_value\":{\"currency\":\"usd\",\"amount\":0},\"tracking_number\":\"123456\",\"label_messages\":{\"reference1\":null,\"reference2\":null,\"reference3\":null},\"external_package_id\":null}]}';
            
            response.setHeader('Content-Type', 'application/json');
            response.setBody(body);
            response.setStatusCode(200);
            return response;

        }

        else{        
            return response;
        }
    }
}