public with sharing class ShipmentCalloutUtility {

    public static HttpResponse makeCallout(String url, String method, String body) {
        
        Credential__mdt creds = [SELECT API_Key__c FROM Credential__mdt WHERE MasterLabel = 'ShipEngine'];

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(url);
        request.setMethod(method);
        request.setHeader('API-Key',creds.API_Key__c);
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');

        if(!String.isEmpty(body)){
        request.setBody(body);
        }   
        
        HttpResponse response = http.send(request);

        return response;
    }
}