({
  init: function (component, event, helper) {
    let action = component.get("c.getCoords");

    action.setParams({
      shipmentId: component.get("v.recordId")
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        const result = response.getReturnValue();

        component.set("v.mapMarkers", [
          {
            location: {
              Latitude: result.Latitude,
              Longitude: result.Longitude
            },

            title: "Current Location",
            description: result.Location
          }
        ]);
      } else {
        console.log("Failed with state: " + state);
      }
    });

    $A.enqueueAction(action);

    component.set("v.zoomLevel", 10);
  }
});