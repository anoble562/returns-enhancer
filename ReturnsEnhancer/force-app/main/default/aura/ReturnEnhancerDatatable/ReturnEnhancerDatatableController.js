({
  fetchRolis: function (component, event, helper) {
    component.set("v.mycolumns", [
      {
        label: "Line Item Number",
        fieldName: "ReturnOrderLineItemNumber",
        type: "text"
      },
      { label: "Asset", fieldName: "AssetName", type: "text" },
      {
        label: "Quantity Returned",
        fieldName: "QuantityReturned",
        type: "text"
      }
    ]);
    var recordId = component.get("v.recordId");
    var action = component.get("c.fetchRoliLines");
    action.setParams({
      returnOrderId: recordId
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        //component.set("v.roliList", response.getReturnValue());
        var rows = response.getReturnValue();
        for (var i = 0; i < rows.length; i++) {
          var row = rows[i];
          if (row.Asset) {
            row.AssetName = row.Asset.Name;
          }
        }
        component.set("v.roliList", rows);
      }
    });
    $A.enqueueAction(action);
  },

  handleRoliAdded: function (component, event, helper) {
    let rolis = event.getParam("selectedRows");
    let updateEvent = component.getEvent("roliAdded");
    updateEvent.setParams({ rolis: rolis });
    updateEvent.fire();
  }
});