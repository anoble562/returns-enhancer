({
  refreshMap: function (component, event, helper) {
    $A.get("e.force:refreshView").fire();
  },

  closeModal: function (component, event, helper) {
    component.set("v.ismodalClicked", false);
    var cmpTarget = component.find("Modalbox");
    var cmpBack = component.find("Modalbackdrop");
    $A.util.removeClass(cmpBack, "slds-backdrop--open");
    $A.util.removeClass(cmpTarget, "slds-fade-in-open");
    console.log("Closing Modal...");
  },

  openModal: function (component, event, helper) {
    component.set("v.ismodalClicked", true);
    var cmpTarget = component.find("Modalbox");
    var cmpBack = component.find("Modalbackdrop");
    $A.util.addClass(cmpTarget, "slds-fade-in-open");
    $A.util.addClass(cmpBack, "slds-backdrop--open");
  }
});