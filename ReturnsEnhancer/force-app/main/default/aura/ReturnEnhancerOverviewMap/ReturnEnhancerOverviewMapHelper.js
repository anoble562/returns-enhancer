({
	getLocations : function(component, filterSetting) {
		let action = component.get("c.getLocations");

    action.setParams({
      returnOrderId: component.get("v.recordId"),
      filterSetting: filterSetting
    });

    action.setCallback(this, function (response) {
      let state = response.getState();
      if (state == "SUCCESS") {
        let result = response.getReturnValue();
        console.log("Result returned: " + JSON.stringify(result));
        component.set("v.mapMarkersData", response.getReturnValue());
        component.set("v.zoomLevel", 3);
        component.set("v.mapCenter", {
          location: {
            Country: "United States"
          }
        });
        component.set("v.markersTitle", "Related Shipments");
        component.set("v.showFooter", true);
      } else {
        console.log("Something went wrong! ");
      }
    });
    $A.enqueueAction(action);
	}
})