({
  doInit: function (component, event, helper) {
  helper.getLocations(component, 'all');
  },

  filterOptions: function (component, event, helper) {
    var setting = event.getParam("value");
    component.set("v.filterSetting", setting);

    helper.getLocations(component, setting);
  }
});