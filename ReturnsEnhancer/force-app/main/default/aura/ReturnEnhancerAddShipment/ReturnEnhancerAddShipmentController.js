({
  handleSubmit: function (component, event, helper) {
    event.preventDefault();

    var newShipment = event.getParam("fields");
    var address = component.get("v.address");

    console.log("Address is this: " + JSON.stringify(address));
    console.log("New shipment fields are: " + JSON.stringify(newShipment));
    console.log("ROLIS are: " + JSON.stringify(component.get("v.addedRolis")));

    helper.createShipment(component, newShipment, address);
  },

  handleSuccess: function (component, event, helper) {
    $A.get("e.force:refreshView").fire();
    let successEvent = component.getEvent("success");
    successEvent.fire();
    console.log("Success!!");
  },

  addressChange: function (component, event, helper) {
    event.preventDefault();
    var address = event.getParams();
    component.set("v.address", address);
  },

  packageTemplateChange: function (component, event, helper) {
    var packageTemplateId = event.getParam("value")[0];
    console.log("Package template: " + packageTemplateId);
    helper.getPackageTemplateOptions(component, packageTemplateId);
  },

  handleRoliAddedEvent: function (component, event, helper) {
    let addedRolis = event.getParam("rolis");
    component.set("v.addedRolis", addedRolis);
    console.log("Rolis Updated");
  }
});