({
  createShipment: function (component, shipment, address) {
    var action = component.get("c.createNewShipment");
    var recordId = component.get("v.recordId");
    //var address = component.get("v.address");
    //
    console.log("Address in helper: " + JSON.stringify(address));
    var shipmentAddress = component.find("shipmentAddress");
    var street = shipmentAddress.get("v.street");
    var city = shipmentAddress.get("v.city");
    var state = shipmentAddress.get("v.province");
    var country = shipmentAddress.get("v.country");
    var postalCode = shipmentAddress.get("v.postalCode");

    var roliList = component.get("v.addedRolis");
    console.log(
      "ROLIS in helper are: " + JSON.stringify(component.get("v.addedRolis"))
    );

    action.setParams({
      shipmentObj: shipment,
      returnOrderId: recordId,
      street: street,
      city: city,
      state: state,
      country: country,
      postalCode: postalCode,
      roliList: roliList
    });

    console.log("Made it to the helper");

    action.setCallback(this, function (response) {
      let state = response.getState();
      if (state === "SUCCESS") {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
          title: "Shipment Saved",
          message: "A new shipment was created."
        });
        resultsToast.fire();
        $A.get("e.force:refreshView").fire();

        let successEvent = component.getEvent("success");
        successEvent.fire();
        
      } else {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
          title: "Save Error",
          message: "There was a problem saving the record."
        });
      }
    });
    $A.enqueueAction(action);
  },
  getPackageTemplateOptions: function (component, packageTemplateId) {
    var action = component.get("c.getPackageTemplateObject");

    action.setParams({
      packageTemplateId: packageTemplateId
    });
    action.setCallback(this, function (response) {
      let state = response.getState();
      if (state === "SUCCESS") {
        console.log(
          "success! response: " + JSON.stringify(response.getReturnValue())
        );

        component.set("v.packageTemplate", response.getReturnValue());
      } else {
        console.log("error: " + JSON.stringify(response.getError()));
      }
    });
    $A.enqueueAction(action);
  }
});